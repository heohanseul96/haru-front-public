import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Login from './screens/Login';
import LoginEmail from './screens/LoginEmail';
import Register from './screens/Register';
import RegisterSuccess from './screens/RegisterSuccess';
import FindUser from './screens/FindUser';
import Chat from './screens/Chat';

const LoginNavigator = createStackNavigator({

  Login: { screen: Login },
  LoginEmail: { screen: LoginEmail, },
  Register: { screen: Register },
  RegisterSuccess: { screen: RegisterSuccess },
  FindUser: { screen: FindUser }

})

const ChatNavigator = createStackNavigator({

  Chat: { screen: Chat }

})

const AppStackNavigator = createStackNavigator({

  LoginView: { screen: LoginNavigator,
               navigationOptions: { header: null }},

  ChatView: { screen: ChatNavigator,
              navigationOptions: { header: null } }

});

export default createAppContainer(AppStackNavigator);