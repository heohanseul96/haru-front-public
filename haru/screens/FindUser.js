import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, TextInput, TouchableOpacity } from 'react-native';

export default class FindUser extends Component {
 
    static navigationOptions = ({navigation}) => ({
        headerLeft: <TouchableOpacity style = {{ width: 10 }} onPress={ () => navigation.navigate('Login') }>
                        <Image source = { require('../image/back.png') }
                               style = {{ marginLeft: 10, width: 20, resizeMode: 'contain' }}/>
                    </TouchableOpacity>
    })

    constructor(props) {
        super(props);

        this.state = {
            signedIn: false,
            loginEmail: '',
            loginID: '',
            loginPW: '',
            token: 'token has not fetched',
          }
    }

    RegisterToServer(){
      fetch('http://13.209.136.125:3000/v1/api/users', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          email: this.state.loginEmail,
          name: this.state.loginID,
          pwd: this.state.loginPW,
        })
      }).then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson);
        return responseJson.success;
      })
      .catch((error) => {
        console.error(error);
      });
    }

    render() {
      const {navigation} = this.props;

      return(
          <View style = { style.Container } behavior='height' enabled>
              <Text>Find</Text>
          </View>
      );
    }
}

const style = StyleSheet.create({

  Container: {
      width: '100%',
      flex: 1,
      flexDirection: 'column'
  },

  LogoContainer: {
      width: '100%',
      flex: 3,
      justifyContent: 'center',
      alignItems: 'center'
  },

  InputContainer: {
      width: '100%',
      flex: 2,
      alignItems: 'center',
      flexDirection: 'column',
      paddingLeft: 50,
      paddingRight: 50,
  },

  InputText:{
      width: '100%',
      height: 50,
      backgroundColor: '#ffffff',
      borderWidth: 1,
      paddingLeft: 20,
      borderRadius: 10,
  },

  loginButton: {
      backgroundColor: 'skyblue',
      width: '100%',
      height: 50,
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 10,
  },

  MenuContainer: {
      flex: 1,
      alignItems: 'center'
  },

});