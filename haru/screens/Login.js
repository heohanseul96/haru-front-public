import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';

export default class Login extends Component {
 
    static navigationOptions = ({
        header: null
    })

    constructor(props) {
        super(props);
    }

    render() {
        const {navigation} = this.props;

        return(
            <View style = { style.Container }>
                <View style = { style.LogoContainer }>
                    <Image source = { require('../image/harulogo.png') } style = { { width: 180, resizeMode: 'contain' } } />
                </View>
                <View style = { style.ButtonContainer }>
                    <TouchableOpacity style = { style.LoginButton } onPress={ () => navigation.navigate('LoginEmail') }>
                        <View style = { { flex: 3, alignItems: 'center', justifyContent: 'center' } }>
                            <Image source = { require('../image/email.png') } style = { style.ButtonLogo } />
                        </View>
                        <View style = { { flex: 8, paddingLeft: 50, justifyContent: 'center' } }>
                            <Text style = { style.ButtonText }>이메일 로그인</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style = { style.RegisterButton } onPress={ () => navigation.navigate('Register') }>
                        <View style = { { flex: 3, alignItems: 'center', justifyContent: 'center' } }>
                            <Image source = { require('../image/emailwhite.png') } style = { style.ButtonLogo } />
                        </View>
                        <View style = { { flex: 8, paddingLeft: 50, justifyContent: 'center' } }>
                            <Text style = { style.ButtonTextReverse }>이메일로 가입</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style = { style.FacebookButton }>
                        <View style = { { flex: 3, alignItems: 'center', justifyContent: 'center' } }>
                            <Image source = { require('../image/facebook.png') } style = { style.ButtonLogo } />
                        </View>
                        <View style = { { flex: 8, paddingLeft: 30, justifyContent: 'center' } }>
                            <Text style = { style.ButtonTextReverse }>페이스북으로 시작</Text>
                        </View>
                    </TouchableOpacity>
                    <View style = { { flex: 3, justifyContent: 'center' } } >
                        <Text style = { { fontSize:10, color: 'grey'}  } >시작과 동시에 HARU의 서비스 약관, 개인정보 취급 방침에 동의하게 됩니다.</Text>
                    </View>
                </View>
            </View>
        );
    }

}

const style = StyleSheet.create({

    Container: {
        width: '100%',
        flex: 1,
        flexDirection: 'column'
    },

    LogoContainer: {
        width: '100%',
        flex: 6,
        alignItems: 'center',
        justifyContent: 'center'
    },

    ButtonContainer: {
        width: '100%',
        flex: 4,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    
    LoginButton: {
        width: '80%',
        flex: 1,
        flexDirection: 'row',
        borderColor: 'black',
        borderWidth: 1,
        borderRadius: 40,
        marginBottom: 5
    },

    RegisterButton: {
        width: '80%',
        flex: 1,
        flexDirection: 'row',
        backgroundColor: 'black',
        borderRadius: 40,
        marginBottom: 5
    },

    FacebookButton: {
        width: '80%',
        flex: 1,
        flexDirection: 'row',
        backgroundColor: '#4267B2',
        borderRadius: 40,
        marginBottom: 5
    },

    GoogleButton: {
        width: '80%',
        flex: 1,
        flexDirection: 'row',
        backgroundColor: '#DB4437',
        borderRadius: 40,
        marginBottom: 5
    },

    ButtonLogo: {
        width: 20,
        resizeMode: 'contain'
    },

    ButtonText: {
        fontSize: 15
    },

    ButtonTextReverse: {
        color: 'white',
        fontSize: 15
    }

});