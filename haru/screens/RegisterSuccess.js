import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';

class RegisterSuccess extends Component {
 
    static navigationOptions = ({navigation}) => ({
      header: null
    })

    constructor(props) {
        super(props);
    }

    render() {
      const {navigation} = this.props;

      return(
          <View style = { style.Container }>
              <View style = { style.LogoContainer }>
                    <Image source = { require('../image/harulogo.png') } style = { { width: 180, resizeMode: 'contain' } } />
              </View>
              <View style = { style.InputContainer }>
                    <Text style = { style.NoticeText }>회원가입이 완료되었습니다.</Text>
                    <TouchableOpacity style = { style.loginButton } onPress={ () => navigation.navigate('LoginEmail')}>
                        <Text style = { { fontSize: 15 } }>로그인하러 가기</Text>
                    </TouchableOpacity>
              </View>
          </View>
      );
    }
}
export default RegisterSuccess;

const style = StyleSheet.create({

  Container: {
      width: '100%',
      flex: 1,
      flexDirection: 'column'
  },

  LogoContainer: {
      width: '100%',
      flex: 6,
      alignItems: 'center',
      justifyContent: 'center'
  },
  
  InputContainer: {
    width: '100%',
    flex: 3,
    alignItems: 'center',
    flexDirection: 'column',
    paddingLeft: 50,
    paddingRight: 50,
},

NoticeText:{
    width: '100%',
    height: 35,
    backgroundColor: '#ffffff',
    textAlign: 'center',
    marginBottom: 5
},

loginButton: {
    backgroundColor: 'skyblue',
    width: '60%',
    height: 35,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
},

});