import React, { Component } from 'react';
import { View, Text, Button, Image, StyleSheet, TouchableOpacity } from 'react-native';
import Modal from "react-native-modal";
import ImagePicker from 'react-native-image-picker';

export default class SendImage extends Component {

    constructor(props) {
        super(props);
        this.state = {
          filePath: {},
          isModalVisible: true
        };
      }

      chooseFile = () => {
        var options = {
          title: 'Select Image',
          customButtons: [
            { name: 'customOptionKey', title: 'Choose Photo from Custom Option' },
          ],
          storageOptions: {
            skipBackup: true,
            path: 'images',
          },
        };

        ImagePicker.showImagePicker(options, response => {
          console.log('Response = ', response);
     
          if (response.didCancel) {
            console.log('User cancelled image picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
            alert(response.customButton);
          } else {
            let source = response;
            // You can also display the image using data:
            // let source = { uri: 'data:image/jpeg;base64,' + response.data };
            this.setState({
              filePath: source,
            });
          }
        });
      };

    render () {
        return (
          <View>
            <Modal isVisible={this.state.isModalVisible} style = {style.Container}>
                <View style = {{flex: 1, flexDirection: 'row', marginTop: 10}}>
                    <View style = {{flex: 9, justifyContent: 'center', alignItems: 'flex-start', paddingLeft: 20}}>
                        <Text style = {{fontSize: 20}}>이미지 전송</Text>
                    </View>
                    <TouchableOpacity onPress={() => this.setState({ isModalVisible: !this.state.isModalVisible})} style = {{flex: 1, justifyContent:'center', alignItems: 'center', paddingRight: 20}}>
                        <Image source = { require('../image/close.png') } style = { style.CancelImage }/>
                    </TouchableOpacity>
                </View>
                <View style = {{flex: 15}}>
                    {/*<Image 
                    source={{ uri: this.state.filePath.path}} 
                    style={{width: 100, height: 100}} />*/}
                    <Image
                        source={{ uri: this.state.filePath.uri }}
                        style={{ width: 250, height: 250 }}
                    />
                    <Button title="Choose File" onPress={this.chooseFile.bind(this)} />
                </View>
            </Modal>
          </View>
        )
    }

}

const style = StyleSheet.create({

    Container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'white',
    },

    MenuImage: {
        width: 50,
        height: 50,
        resizeMode: 'contain',
    },

    CancelImage: {
        width: 20,
        height: 20,
        resizeMode: 'contain',
    },
});