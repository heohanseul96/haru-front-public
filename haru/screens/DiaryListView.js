import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image
} from 'react-native';
import { CardList } from 'react-native-card-list';
 
const cards = [
  {
    id: "0",
    title: "나홀로 부산여행기",
    picture: require('../image/busan.jpg'),
    content: 
      <ScrollView>
        <View style = {{ width: '100%', paddingBottom: 20, alignItems: 'center', justifyContent: 'center'}}>
          <Text style = {{fontSize: 18}}>2019년 8월 23일부터</Text>
          <Text style = {{fontSize: 18}}>2019년 8월 25일까지의 기록</Text>
        </View>
        <View style = {{ width: '100%', height: 20, justifyContent: 'center', alignItems: 'center'}}>
          <Image source = { require('../image/diarylogo.png') } style = {{ resizeMode: 'center', opacity: 0.8 }}/>
        </View>

        <View style = {{alignItems: 'center', paddingTop: 20}}>
          <Text style = {{fontSize: 20, fontWeight: 'bold'}}>2019년 8월 23일</Text>
          <Text style = {{fontSize: 16}}> </Text>
          <Text style = {{fontSize: 12, backgroundColor: '#90d8f9'}}>7:38AM</Text>
          <Text style = {{fontSize: 16}}>"이제 드디어 출발한다!"</Text>
          <Text style = {{fontSize: 16}}>"혼자서 여행 가는 거 처음이라 너무 설레 ㅎㅎ"</Text>
          <Text style = {{fontSize: 16}}> </Text>
          <Text style = {{fontSize: 12, backgroundColor: '#90d8f9'}}>10:15AM</Text>
          <Text style = {{fontSize: 16}}>"부산역 도착!! 확실히 서울보다 따뜻하넹"</Text>
        </View>
        <View style = {{justifyContent: 'center', alignItems: 'center'}}>
          <Image source = { require('../image/busan.jpg') } style = {{width: 300, height: 300, resizeMode: 'center'}}/>
        </View>
        <View style = {{alignItems: 'center'}}>
          <Text style = {{fontSize: 12, backgroundColor: '#90d8f9'}}>10:27AM</Text>
          <Text style = {{fontSize: 16}}>"택시타고 해운대 먼저 왔어!!"</Text>
          <Text style = {{fontSize: 16}}>"바다 너무 오랜만이야 ㅠㅠ 너무 좋다.."</Text>
          <Text style = {{fontSize: 16}}>"이렇게 좋은 풍경 볼 때는 혼자인게 아쉽긴 하당"</Text>
          <Text style = {{fontSize: 16}}> </Text>
          <Text style = {{fontSize: 12, backgroundColor: '#90d8f9'}}>11:05AM</Text>
          <Text style = {{fontSize: 16}}>"배고파서 근처 한식집으로 왔어!"</Text>
        </View>
        <View style = {{justifyContent: 'center', alignItems: 'center'}}>
          <Image source = { require('../image/food.jpg') } style = {{width: 300, height: 300, resizeMode: 'center'}}/>
        </View>
      </ScrollView>
  },
  {
    id: "1",
    title: "간만의 서울 나들이",
    picture: require('../image/seoul.jpg'),
    content: <Text>Wheat Field with Cypresses</Text>
  },
  {
    id: "2",
    title: "나만 알고싶은 보은",
    picture: require('../image/boeun.jpg'),
    content: <Text>Bedroom in Arles</Text>
  },
  {
    id: "3",
    title: "제주도 여행, 그 세번째",
    picture: require('../image/jeju.jpg'),
    content: <Text>Bedroom in Arles</Text>
  }
]
 
export default class DiaryListView extends Component {
  render() {
    return (
      <View style={styles.container}>
        <CardList cards={cards} />
      </View>
    );
  }
}
 
const styles = StyleSheet.create({

  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },

  ContentImage: {
    width: 100,
    height: 100,
    resizeMode: 'contain',
  },

})