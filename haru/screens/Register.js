import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, TextInput, TouchableOpacity } from 'react-native';

export default class Register extends Component {
 
    static navigationOptions = ({navigation}) => ({
        headerLeft: <TouchableOpacity style = {{ width: 10 }} onPress={ () => navigation.navigate('Login') }>
                        <Image source = { require('../image/back.png') }
                               style = {{ marginLeft: 10, width: 20, resizeMode: 'contain' }}/>
                    </TouchableOpacity>
    })

    constructor(props) {
        super(props);

        this.state = {
            signedIn: false,
            loginEmail: '',
            loginID: '',
            loginPW: '',
            token: 'token has not fetched',
            nextAvailable: false,
          }
    }

    RegisterToServer(){
      fetch('http://13.209.136.125:3000/v1/api/users', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          email: this.state.loginEmail,
          name: this.state.loginID,
          pwd: this.state.loginPW,
        })
      }).then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson);
        this.setState({nextAvailable: responseJson.success});
        this.RegisterSuccess();
      })
      .catch((error) => {
        console.error(error);
      });
    }

    RegisterSuccess(){
      if(this.state.nextAvailable){
          this.GoToNext();
      }
      else{
          alert('회원가입에 실패하였습니다.');
      }
    }

    GoToNext(){
      const {navigation} = this.props;

      navigation.navigate('RegisterSuccess')
  }

    render() {
      const {navigation} = this.props;

      return(
          <View style = { style.Container } behavior='height' enabled>
              <View style = { style.LogoContainer }>
                  <Image source = { require('../image/harulogo.png') } style = { { width: 180, resizeMode: 'contain' } } />
              </View>
              <View style = { style.InputContainer }>
                  <TextInput placeholder = {'email'} style = { style.InputText } onChangeText={(text) => this.setState({loginEmail: text})}/>
                  <TextInput placeholder = {'name'} style = { style.InputText } onChangeText={(text) => this.setState({loginID: text})}/>
                  <TextInput placeholder = {'password'} style = { style.InputText } onChangeText={(text) => this.setState({loginPW: text})}/>
                  <TouchableOpacity style = { style.loginButton } onPress={ () => this.RegisterToServer()}>
                      <Text style = { { fontSize: 20 } }>회원가입</Text>
                  </TouchableOpacity>
              </View>
          </View>
      );
    }
}

const style = StyleSheet.create({

  Container: {
      width: '100%',
      flex: 1,
      flexDirection: 'column'
  },

  LogoContainer: {
      width: '100%',
      flex: 3,
      justifyContent: 'center',
      alignItems: 'center'
  },

  InputContainer: {
      width: '100%',
      flex: 2,
      alignItems: 'center',
      flexDirection: 'column',
      paddingLeft: 50,
      paddingRight: 50,
  },

  InputText:{
      width: '100%',
      height: 35,
      backgroundColor: '#ffffff',
      borderWidth: 1,
      paddingLeft: 20,
      borderRadius: 10,
      marginBottom: 5
  },

  loginButton: {
      backgroundColor: 'skyblue',
      width: '100%',
      height: 35,
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 10,
  },

  MenuContainer: {
      flex: 1,
      alignItems: 'center'
  },

});