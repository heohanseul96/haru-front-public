import React, { Component } from 'react';
import { View, Text, Image, FlatList, StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import Modal from "react-native-modal";
import SearchResult from './SearchResult';

export default class SearchScreen extends Component {

    state = {
        isModalVisible: true,
        isResultVisible: false,
        text: '어디로 떠나시겠어요?',
        keyword:"",
        data:[],
        page:1,
        refreshing: false
    };

    toggleResult() {
        this.setState({ isResultVisible: !this.state.isResultVisible });
    }

    callApi_area = () => {
        var keyword = this.state.keyword;
        keyword =encodeURI(keyword);
        var numOfRows ="10";

        
        if(keyword=="undefined"){
            var url = "http://api.visitkorea.or.kr/openapi/service/rest/KorService/searchKeyword?ServiceKey=eH5vEAGDbkgYsPKO%2FfpNJXL50loz2uJzmcFgQi9grUxQg089uxVcDUY5WM7UA%2BtbgcBfwXxuy2OPX9%2B3zpcJ%2FA%3D%3D&keyword="
                +this.state.keyword+"&MobileOS=ETC&MobileApp=TestApp&_type=json"; 
        }
        else{
            var url = "http://api.visitkorea.or.kr/openapi/service/rest/KorService/searchKeyword?ServiceKey=akViE1SfP4Z15GF9sg4WKBrf3J4D4%2FAbUUe4Fp6RTSAyeTYHF6CMizo0HGbwiwaeixPhSzVqaXoJatcHCfkVhQ%3D%3D&keyword="
            +this.state.keyword+"&numOfRows="+numOfRows+"&pageNo="+this.state.page+"&MobileOS=ETC&MobileApp=TestApp&_type=json"; 
        }
        console.log('before fetch')
        console.log(this.state.keyword)
        console.log(keyword)
        fetch(url)
          .then(res => res.json())
          .then(json => json.response.body.items.item)
          .then(data => this.setState({
           //data: json.response.body.items.item,
           data: this.state.refreshing? data : this.state.data.concat(data),
           page: this.state.page + 1,
           refreshing: false,
          })
    )}

    EndReach(){
        alert("로그인이 필요한 서비스 입니다.");
    }

    _handleLoadMore = () => {
        this.callApi_area();
    }

    _handleRefresh = () => {
        this.setState({
          refreshing: true,
          page: 3,
        }, this.callApi_area());
    }

    _renderItem = ({item}) => {
        return(
            <TouchableOpacity 
                            onPress={() => {alert("로그인이 필요한 서비스 입니다.");}}>
                <Image  source={{uri:item.firstimage}} style={{width:'100%', height:100,marginTop:20,opacity:0.7}}/>  
                <View style={{position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, justifyContent: 'flex-end', alignItems: 'flex-start'}}>
                <Text style={{fontSize:18,color:'black',paddingBottom:5,opacity:0.7,paddingLeft:5}}>{item.addr1}</Text>
                <Text style={{fontSize:20,fontWeight:'bold',color:'black',paddingBottom:5,paddingLeft:5}}>{item.title}</Text> 
                </View>
            </TouchableOpacity> 
        );
    }
    
    renderResultList() {
        if(this.state.isResultVisible){
            return(
                <FlatList
                  data={this.state.data}
                  renderItem={this._renderItem}
                  keyExtractor={(item,index)=>index.toString()}
                  onEndReached={this._handleLoadMore}
                  onEndReachedThreshold={1}
                  refreshing={this.state.refreshing}
                  onRefresh={this._handleRefresh}
                 />
            );
        } else{
            return null;
        }
    }

    searchEvent(){
        this.callApi_area();
        this.setState({isResultVisible: true});
    }

    render () {
        return (
          <View>
            <Modal isVisible={this.state.isModalVisible} style = {style.Container}>
                <View style = {{flex: 1, flexDirection: 'row', marginTop: 10}}>
                    <View style = {{flex: 9, justifyContent: 'center', alignItems: 'flex-start', paddingLeft: 20}}>
                        <Text style = {{fontSize: 30}}>검색하기</Text>
                    </View>
                    <TouchableOpacity onPress={() => this.setState({ isModalVisible: !this.state.isModalVisible})} style = {{flex: 1, justifyContent:'center', alignItems: 'center', paddingRight: 20}}>
                        <Image source = { require('../image/close.png') } style = { style.CancelImage }/>
                    </TouchableOpacity>
                </View>
                <View style = { style.searchPart }>
                    <View style = { {flex: 7} }>
                        <TextInput style = { style.searchInput }
                            value={this.state.keyword}
                            placeholder={this.state.text}
                            onChangeText={(text) => this.setState({ keyword: text })}
                        />
                    </View>
                    <View style = { {flex: 1} }>
                        <TouchableOpacity style = { style.searchButton } name='ios-search' onPress={ () => this.searchEvent() }/>
                    </View>
                </View>
                <View isVisible = {this.state.isResultVisible} style = {{ flex: 5}}>
                    {this.renderResultList()}
                </View>
            </Modal>
          </View>
        )
    }

}

const style = StyleSheet.create({

    Container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'white',
    },

    MenuImage: {
        width: 50,
        height: 50,
        resizeMode: 'contain',
    },

    CancelImage: {
        width: 30,
        height: 30,
        resizeMode: 'contain',
    },

    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },

    searchPart: {
        flex: 1,
        width: '100%',
        alignItems: 'baseline',
        flexDirection: 'row',
        alignContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        paddingLeft: '5%',
        paddingRight: '5%',
    },

    searchInput: {
        height: 40, 
        borderColor: 'gray', 
        borderWidth: 1,
        paddingLeft: '2%',
    },

    searchButton: {
        borderWidth: 1,
        height: 40,
        alignItems: 'center',
        textAlignVertical: 'center',
        textAlign: 'center',
    },
});