import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList,TouchableOpacity,Image,Dimensions} from 'react-native';
import { Icon } from 'native-base';

var {width,height} = Dimensions.get('window')

export default class HomeResultScreen extends Component {
    constructor(props) {

        super(props);
    
        this.state = {
          data:[],
          page:1,
          refreshing:false
        }
    }
    
    callApi_area = () => {
        const {navigation} = this.props;
        var keyword = navigation.getParam("keyword");
        keyword =encodeURI(keyword);
        var numOfRows ="10";
        var contentTypeId = "12"; //관광지
        var areaCode = navigation.getParam("select");
        var arrange = "P"; //정렬기준 O=제목순, P=조회순, Q=수정일순, R=생성일순
        
        if(keyword=="undefined"){
            var url = "http://api.visitkorea.or.kr/openapi/service/rest/KorService/areaBasedList?ServiceKey=eH5vEAGDbkgYsPKO%2FfpNJXL50loz2uJzmcFgQi9grUxQg089uxVcDUY5WM7UA%2BtbgcBfwXxuy2OPX9%2B3zpcJ%2FA%3D%3D"
                +"&areaCode="+areaCode+"&numOfRows="+numOfRows+"&pageNo="+this.state.page+"&contentTypeId="+contentTypeId+"&arrange="+arrange+"&MobileOS=ETC&MobileApp=TestApp&_type=json"; 
        }
        else{
            var url = "http://api.visitkorea.or.kr/openapi/service/rest/KorService/searchKeyword?ServiceKey=eH5vEAGDbkgYsPKO%2FfpNJXL50loz2uJzmcFgQi9grUxQg089uxVcDUY5WM7UA%2BtbgcBfwXxuy2OPX9%2B3zpcJ%2FA%3D%3D"
            +"&numOfRows="+numOfRows+"&pageNo="+this.state.page+"&keyword="+keyword+"&MobileOS=ETC&MobileApp=TestApp&_type=json"; 
        }
        fetch(url)
          .then(res => res.json())
          .then(json => json.response.body.items.item)
          .then(data => this.setState({
           //data: json.response.body.items.item,
           data: this.state.refreshing? data : this.state.data.concat(data),
           page: this.state.page + 1,
           refreshing:false,
          })
    )}
    
    componentDidMount() {

        this.callApi_area();
    
    }

    _handleLoadMore = () => {
        this.callApi_area();
    }

    _handleRefresh = () => {
        this.setState({
          refreshing: true,
          page: 3,
        }, this.callApi_area);
    }
    
    static navigationOptions = ({navigation}) => ({
        headerRight: (
            <Icon name='ios-home' style={{ paddingRight:10 }} onPress={() => navigation.navigate('Home')}/>
        ),
        title: '해달',
        headerTitleStyle: {
            alignSelf: 'center',
            textAlign: "center",
            justifyContent: 'center',
            flex: 1,
            fontWeight: '900',
            fontSize: 35,
            color: '#0066cc',
            textAlignVertical: 'center',
        },
    })
    
    // _renderItem = ({item}) => (
    //     <View style={{borderBottomWidth:1, marginBottom: 10}}>
    //       <ImageBackground source={{ uri: item.firstimage }} style={{width:(width), height:(height/3),marginTop:20,opacity:0.5,justifyContent: 'flex-end'}}>
    //         <Text style={{fontSize:15}}>{item.title}</Text>
    //       </ImageBackground>
    //     </View>
    // );

    _renderItem = ({item}) => (
        <TouchableOpacity 
                        onPress={() => {alert("로그인이 필요한 서비스 입니다.");}}>
            <Image  source={{uri:item.firstimage}} style={{width:(width), height:(height/3),marginTop:20,opacity:0.7}}/>  
            <View style={{position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, justifyContent: 'flex-end', alignItems: 'flex-start'}}>
              <Text style={{fontSize:18,color:'black',paddingBottom:5,opacity:0.7,paddingLeft:5}}>{item.addr1}</Text>
              <Text style={{fontSize:20,fontWeight:'bold',color:'black',paddingBottom:5,paddingLeft:5}}>{item.title}</Text> 
            </View>
        </TouchableOpacity> 
    );

    // renderSectionOne = ({item}) => {
        
    //     return this.state.data.map((item,index) => {
    //         return (
    //             <TouchableOpacity key={index}
    //                               onPress={() => {alert("로그인이 필요한 서비스 입니다.");}}>
    //                 <Image  source={{uri:item.firstimage}} style={{width:(width), height:(height/3),marginTop:20,opacity:0.7}}/>  
    //                 <View style={{position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, justifyContent: 'flex-end', alignItems: 'flex-start'}}>
    //                   <Text style={{fontSize:18,color:'black',paddingBottom:5,opacity:0.7,paddingLeft:5}}>{item.addr1}</Text>
    //                   <Text style={{fontSize:20,fontWeight:'bold',color:'black',paddingBottom:5,paddingLeft:5}}>{item.title}</Text> 
    //                 </View>
    //             </TouchableOpacity>
    //         )
    //     })
    // }

    render() {
        return (
            <View style={style.container}>
                <View style = { style.title }>
                    {/* <Text style = { style.titleText }>{this.state.data }</Text> */}
                </View>
                <FlatList
                  data={this.state.data}
                  renderItem={this._renderItem}
                  keyExtractor={(item,index)=>index.toString()}
                  onEndReached={this._handleLoadMore}
                  onEndReachedThreshold={1}
                  refreshing={this.state.refreshing}
                  onRefresh={this._handleRefresh}
                 />
            </View>
        );
    }
}

const style = StyleSheet.create({

    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
    },

    title: {
        flex: 1,
        width: '100%',
        textAlignVertical: 'center',
        paddingLeft: 10,
        paddingTop: 10,
    },
    
    titleText: {
        fontSize: 20,
        textAlignVertical: 'center',
    },
    
    result: {
        flex: 7,
        width: '100%',
    }

});
