import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import Modal from "react-native-modal";
import DiaryListView from './DiaryListView';

export default class DiaryList extends Component {

    state = {
        isModalVisible: true
    };

    render () {
        return (
          <View>
            <Modal isVisible={this.state.isModalVisible} style = {style.Container}>
                <View style = {{flex: 1, flexDirection: 'row', marginTop: 10}}>
                    <View style = {{flex: 9, justifyContent: 'center', alignItems: 'flex-start', paddingLeft: 20}}>
                        <Text style = {{fontSize: 20}}>다이어리 목록</Text>
                    </View>
                    <TouchableOpacity onPress={() => this.setState({ isModalVisible: !this.state.isModalVisible})} style = {{flex: 1, justifyContent:'center', alignItems: 'center', paddingRight: 20}}>
                        <Image source = { require('../image/close.png') } style = { style.CancelImage }/>
                    </TouchableOpacity>
                </View>
                <View style = {{flex: 15}}>
                    <DiaryListView/>
                </View>
            </Modal>
          </View>
        )
    }

}

const style = StyleSheet.create({

    Container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'white',
    },

    MenuImage: {
        width: 50,
        height: 50,
        resizeMode: 'contain',
    },

    CancelImage: {
        width: 20,
        height: 20,
        resizeMode: 'contain',
    },
});