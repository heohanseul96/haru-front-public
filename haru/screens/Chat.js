// App.js
import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { GiftedChat, Composer } from "react-native-gifted-chat";
import { Dialogflow_V2 } from 'react-native-dialogflow';
import { dialogflowConfig } from './env';
import { Avatar } from 'react-native-elements';
import { isSameUser } from 'react-native-gifted-chat/lib/utils';

const BOT_USER = {
  _id: 2,
  name: 'FAQ Bot',
  avatar: require('../image/logo.png')
};

export default class Chat extends Component {

  static navigationOptions = ({navigation}) => ({
    headerLeft:
      <View style = {{ flexDirection: 'row', paddingLeft: 10, justifyContent: 'center', textAlignVertical: 'center'}}>
        <Avatar
          rounded
          source = { require('../image/logo.png') }
        />
        <Text style = {{fontSize: 25, paddingLeft: 10, justifyContent: 'center', textAlignVertical: 'center'}}>하루</Text>
      </View>,
    headerRight: null,
    paddingLeft: 100
  })

  constructor(props) {
    super(props);

    this.state = {
      token: '',
      saveText: '',
      saveDate: '',
      saveIntent: '',
      filePath: null,
      messages: [
        {
          _id: 1,
          text: `안녕하세요! 하루에요:)\n\n무슨 말이든 해주세요.`,
          createdAt: new Date(),
          user: BOT_USER
        }
      ]
    }
  }  

  componentDidMount() {
    Dialogflow_V2.setConfiguration(
      dialogflowConfig.client_email,
      dialogflowConfig.private_key,
      Dialogflow_V2.LANG_ENGLISH_US,
      dialogflowConfig.project_id
    );
  }

  onSend(messages = []) {
    //this.setState({saveText: messages[0].text});
    //console.log(saveText);
    console.log(messages);

    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, messages)
    }));

    let message = messages[0].text;

    console.log(message);

    Dialogflow_V2.requestQuery(
      message,
      result => this.handleGoogleResponse(result),
      error => console.log(error)
    );
  }

  /*SendImage(filePath){
    const message = {};
    
    message._id = this.messageIdGenerator();
                    message.createdAt = Date.now();
                    message.user = {
                        _id: user._id,
                        name: `${user.firstName} ${user.lastName}`,
                        avatar: user.avatar
                    };
                    message.image = response.headers.Location;
                    message.messageType = "image";
  }*/
  
  /*SaveToServer(){
    fetch('http://13.209.136.125:3000/v1/api/auth/login', {
          method: 'POST',
          headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'x-access-token': this.state.token,
          },
          body: JSON.stringify({ 
            SentDate: "2019-01-01", 
            Text: "SEONGIM", 
            DiaryId: "1", 
            Intent:"긍정"})
        }).then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson);
          this.setState({nextAvailable: responseJson.success});
          this.setState({token:responseJson.data});
          this.GoToNext();
        })
        .catch((error) => {
          console.error(error);
        });
  }*/

  handleGoogleResponse(result) {
    this.setState({saveIntent: result.queryResult.intent.displayname});

    let text = result.queryResult.fulfillmentMessages[0].text.text[0];

    this.sendBotResponse(text);
  }

  onSend(messages = []) {
    //this.setState({saveText: messages[0].text});
    //console.log(saveText);

    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, messages)
    }));

    let message = messages[0].text;

    Dialogflow_V2.requestQuery(
      message,
      result => this.handleGoogleResponse(result),
      error => console.log(error)
    );
  }

  sendBotResponse(text) {
    let msg = {
      _id: this.state.messages.length + 1,
      text,
      createdAt: new Date(),
      user: BOT_USER
    };

    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, [msg])
    }));
  }

  render() {
    const {navigation} = this.props;
    this.state.token = navigation.getParam("token");

    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <GiftedChat
          messages={this.state.messages}
          onSend={messages => this.onSend(messages)}
          user={{
            _id: 1
          }}
          renderComposer = {false}
        />
      </View>
    );
  }
}