import React, { Component } from 'react';
import { View, Image, StyleSheet, TextInput, Text, TouchableOpacity } from 'react-native';
import moment from 'moment';
require('moment-timezone'); 

export default class LoginEmail extends Component {

    static navigationOptions = ({navigation}) => ({
        headerLeft: <TouchableOpacity style = {{ width: 10 }} onPress={ () => navigation.navigate('Login') }>
                        <Image source = { require('../image/back.png') }
                               style = {{ marginLeft: 10, width: 20, resizeMode: 'contain' }}/>
                    </TouchableOpacity>
    })

    constructor(props) {
        super(props);

        this.state = {
            signedIn: false,
            loginID: null,
            loginPW: null,
            loginDate: null,
            token: 'token has not fetched',
            diaryID: null,
            nextAvailable: false,
          }
    }

    formatTime({timestamp}){ 
        const formattedDT = moment.tz(timestamp, 'Asia/Seoul').format('YYYY-MM-DD HH:mm ZZ'); 
        return formattedDT; 
    }
    
    LoginToServer(){
        var date = moment();

        this.formatTime(date);

        fetch('http://13.209.136.125:3000/v1/api/auth/login', {
          method: 'POST',
          headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            email: this.state.loginID,
            pwd: this.state.loginPW,
          })
        }).then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson);
          this.setState({nextAvailable: responseJson.success});
          this.setState({token: responseJson.data});
          this.setState({loginDate: date});
          console.log(this.state.loginDate);
          this.LoginSuccess();
        })
        .catch((error) => {
          console.error(error);
        });
    }

    LoginSuccess(){
        if(this.state.nextAvailable){
            this.GetDiary();
        }
        else{
            alert('로그인에 실패하였습니다.');
        }
    }

    GetDiary(){
        fetch('http://13.209.136.125:3000/v1/api/diary', {
          method: 'POST',
          headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'x-access-token': this.state.token,
          },
          body: JSON.stringify({
            loginDate: this.state.loginDate,
          })
        }).then((response) => response.json())
        .then((responseJson) => {
          this.setState({nextAvailable: false});
          console.log(responseJson);
          this.setState({diaryID: responseJson.data.id});
          console.log(this.state.diaryID);
          this.GoToNext();
        })
        .catch((error) => {
          console.error(error);
        });
    }

    GoToNext(){
        const {navigation} = this.props;
        
        navigation.navigate('Chat',{"token":this.state.token, "diaryID": this.state.diaryID})
    }

    render() {
        const {navigation} = this.props;
        return(
            <View style = { style.Container } behavior='height' enabled>
                <View style = { style.LogoContainer }>
                    <Image source = { require('../image/harulogo.png') } style = { { width: 180, resizeMode: 'contain' } } />
                </View>
                <View style = { style.InputContainer }>
                    <TextInput placeholder = {'email'} style = { style.InputText } onChangeText={(text) => this.setState({loginID: text})}/>
                    <TextInput placeholder = {'password'} style = { style.InputText } onChangeText={(text) => this.setState({loginPW: text})}/>
                    <TouchableOpacity style = { style.loginButton } onPress={ () => this.LoginToServer()}>
                        <Text style = { { fontSize: 15 } }>로그인</Text>
                    </TouchableOpacity>
                    <View style = {{flex: 2, width: '100%', flexDirection: 'row', paddingTop: 5, }}>
                        <TouchableOpacity style = { style.MenuContainer } onPress={ () => navigation.navigate('Register') }>
                            <Text style = { { fontSize: 13, color: 'gray' } }>회원가입</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style = { style.MenuContainer } onPress={ () => navigation.navigate('FindUser') }>
                            <Text style = { { fontSize: 13, color: 'gray'} }>ID/PW 찾기</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}

const style = StyleSheet.create({

    Container: {
        width: '100%',
        flex: 1,
        flexDirection: 'column'
    },

    LogoContainer: {
        width: '100%',
        flex: 3,
        justifyContent: 'center',
        alignItems: 'center'
    },

    InputContainer: {
        width: '100%',
        flex: 2,
        alignItems: 'center',
        flexDirection: 'column',
        paddingLeft: 50,
        paddingRight: 50,
    },

    InputText:{
        width: '100%',
        height: 35,
        backgroundColor: '#ffffff',
        borderWidth: 1,
        paddingLeft: 20,
        borderRadius: 10,
        marginBottom: 5
    },

    loginButton: {
        backgroundColor: 'skyblue',
        width: '100%',
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
    },

    MenuContainer: {
        flex: 1,
        alignItems: 'center'
    },

});